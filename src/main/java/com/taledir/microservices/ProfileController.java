package com.taledir.microservices;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import com.taledir.microservices.captcha.ICaptchaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class ProfileController {

	Logger logger = LoggerFactory.getLogger(ProfileController.class);
	
	@Autowired
	ProfileRepository profileRepository;

	@Autowired
  ProfileService profileService;
  
  @Autowired
  private ICaptchaService captchaService;

	@GetMapping("/users/{username}")
	public Profile retrievePublicProfile(@PathVariable String username) {

		Profile profile = profileRepository.findOneByUsername(username);
		//TODO:  Return DTO here, so that only public information can be seen
		
		return profile;
	}

	@GetMapping("/profiles/current")
	public Profile retrieveCurrentProfile(
		@CookieValue(value = "token", defaultValue = "") String token) {

		if(token == null || token.length() == 0) {
			throw new UserBadRequestException("Cannot access profile, please try to login first");
		}
		String username = profileService.getTokenUsername(token);

		if(username == null || username.length() == 0) {
			throw new UserBadRequestException("Cannot access profile, please try to login first");
		}

		Profile profile = profileRepository.findOneByUsername(username);
		
		return profile;
	}

	@PatchMapping("/profiles/current")
	public Profile updateCurrentProfile(
    @CookieValue(value = "token", defaultValue = "") String token,
    @Valid @RequestBody ProfileUpdateDto profileUpdateDto, BindingResult bindingResult) {

		if(token == null || token.length() == 0) {
			throw new UserBadRequestException("Cannot access profile, please try to login first");
    }

		if(bindingResult.hasErrors()) {
			if(bindingResult.getFieldError() != null) {
				throw new UserBadRequestException(bindingResult.getFieldError().getDefaultMessage());
			}
		}

		String username = profileService.getTokenUsername(token);

		if(username == null || username.length() == 0) {
			throw new UserBadRequestException("Cannot access profile, please try to login first");
		}

    Profile profile = profileRepository.findOneByUsername(username);

    if(profileUpdateDto.getEmail() != null && 
        !profileUpdateDto.getEmail().equals(profile.getEmail())) {

      if(profileRepository.findOneByEmail(profileUpdateDto.getEmail()) != null) {
        throw new UserAlreadyExistsException("Unable to update profile, new email already exists.");
      }

      try {
        ResponseEntity<Token> response = profileService.updateAccountEmail(profileUpdateDto.getEmail(), token);
        if(response == null || !response.getStatusCode().equals(HttpStatus.OK) ) {
          throw new UserBadRequestException("Unable to update account email");
        }
      } catch(HttpClientErrorException e) {
        throw new UserBadRequestException(e.getResponseBodyAsString());
      }
      profile.setEmail(profileUpdateDto.getEmail());
    }
    if(profileUpdateDto.getName() != null) {
      profile.setName(profileUpdateDto.getName());
    }
    if(profileUpdateDto.getAboutDescription() != null) {
      profile.setAboutDescription(profileUpdateDto.getAboutDescription());
    }
    
    Profile updatedProfile = profileRepository.save(profile);
		
		return updatedProfile;
	}


	@PostMapping("/profiles/create")
	public ResponseEntity<Object> createProfile(@Valid @RequestBody CreateProfileDto createProfileDto, BindingResult bindingResult) {

		if(bindingResult.hasErrors()) {
			if(bindingResult.getFieldError() != null) {
				throw new UserBadRequestException(bindingResult.getFieldError().getDefaultMessage());
			}
		}

    captchaService.processResponse(createProfileDto.getCaptchaResponse());

		if(profileRepository.findOneByUsername(createProfileDto.getUsername()) != null) {
			throw new UserAlreadyExistsException("Unable to create profile, username already exists.");
		}

		if(profileRepository.findOneByEmail(createProfileDto.getEmail()) != null) {
			throw new UserAlreadyExistsException("Unable to create profile, email already exists.");
		}

		ResponseEntity<Token> response;
		try {
			response = profileService.registerAccount(
        createProfileDto.getUsername(),
        createProfileDto.getEmail(),
        createProfileDto.getPassword());
		} catch(HttpClientErrorException e) {
			Map<String, Object> errorBody = new HashMap<String, Object>();
      errorBody.put("status", HttpStatus.BAD_REQUEST.value());
      errorBody.put("error", "Bad Request");
			errorBody.put("message", e.getResponseBodyAsString());
			return ResponseEntity.badRequest().body(errorBody);
		}

		if(response == null || !response.getStatusCode().equals(HttpStatus.OK) ) {
			throw new UserBadRequestException("Unable to create account");
		}

		Profile profile = new Profile(createProfileDto.getUsername(),
      createProfileDto.getName(), createProfileDto.getEmail(), 
      createProfileDto.getAboutDescription());

		Profile savedProfile = profileRepository.save(profile);

		//TODO: Concern about using event-driven vs REST-driven coupling to Story Aggregate

		URI location = ServletUriComponentsBuilder
			.fromPath("/profiles")
			.path("/{id}")
			.buildAndExpand(savedProfile.getUsername()).toUri();
			
    List<String> cookieList = response.getHeaders().get(HttpHeaders.SET_COOKIE);
    String[] cookieArray = cookieList.toArray(new String[0]);

    return ResponseEntity.created(location)
			.header(HttpHeaders.SET_COOKIE, cookieArray)
			.body(response.getBody());
	}
}
