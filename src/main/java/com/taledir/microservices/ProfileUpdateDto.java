package com.taledir.microservices;

import javax.validation.constraints.Email;

public class ProfileUpdateDto {

	private String name;

  @Email(message = "Please enter a valid email address")
	private String email;

	private String aboutDescription;


	public ProfileUpdateDto(String name, String email, 
		String aboutDescription) {
		this.name = name;
		this.email = email;
		this.aboutDescription = aboutDescription;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.toLowerCase().trim();
	}

	public String getAboutDescription() {
		return aboutDescription;
	}

	public void setAboutDescription(String aboutDescription) {
		this.aboutDescription = aboutDescription;
	}
}