package com.taledir.microservices;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ProfileService {

    @Value("${spring.security.user.name}")
    private String authServiceUsername;

    @Value("${spring.security.user.password}")
    private String authServicePassword;

	Logger logger = LoggerFactory.getLogger(ProfileService.class);

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

  protected ResponseEntity<Token> updateAccountEmail(String email, String token) {

		HttpHeaders reqHeaders = new HttpHeaders();
    reqHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
    Map<String, String> body = new HashMap<String, String>();
    body.put("email", email);
    body.put("token", token);

    HttpEntity<Map<String, String>> requestEntity = 
      new HttpEntity<Map<String, String>>(body, reqHeaders);
  
    //TODO:  Update to use configurable path, or same path as existing service
		ResponseEntity<Token> responseEntity = 
			restTemplate().exchange("http://taledir-test.com/auth/updateEmail", 
      HttpMethod.POST, requestEntity, Token.class);

		return responseEntity;
	}

  protected ResponseEntity<Token> registerAccount(String username, String email, String password) {

		HttpHeaders reqHeaders = new HttpHeaders();
    reqHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
    Map<String, String> body = new HashMap<String, String>();
    body.put("username", username);
    body.put("email", email);
    body.put("password", password);

    HttpEntity<Map<String, String>> requestEntity = 
      new HttpEntity<Map<String, String>>(body, reqHeaders);
  
    //TODO:  Update to use configurable path, or same path as existing service
		ResponseEntity<Token> responseEntity = 
			restTemplate().exchange("http://taledir-test.com/auth/create", 
      HttpMethod.POST, requestEntity, Token.class);

		return responseEntity;
	}

	protected String getTokenUsername(String token) {
		HttpHeaders reqHeaders = new HttpHeaders();
		reqHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		reqHeaders.setBasicAuth(authServiceUsername, authServicePassword);
		
		MultiValueMap<String, String> body= new LinkedMultiValueMap<String, String>();
		body.add("token", token);
		
		HttpEntity<MultiValueMap<String, String>> requestEntity = 
			new HttpEntity<MultiValueMap<String, String>>(body, reqHeaders);

		ResponseEntity<Object> responseEntity;
		try {
			responseEntity = restTemplate().exchange(
				"http://taledir-test.com/auth/introspect", 
            	HttpMethod.POST, requestEntity, Object.class);

		} catch(HttpClientErrorException e) {
			logger.error("Authorization server username or password is incorrect.  " + 
				 "This will prevent authorization!");
			return null;
		}
		Object oBody = responseEntity.getBody();
		ObjectMapper oMapper = new ObjectMapper();
		Map<String, Object> responseBody = 
			oMapper.convertValue(oBody, new TypeReference<Map<String, Object>>() {});

		Object responseUsername = responseBody.get("username");

		if(responseUsername != null) {
			return responseUsername.toString();
		}
		return null;
	}
}