package com.taledir.microservices;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "profile")
public class Profile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonIgnore
	private Integer id;

	@NaturalId(mutable = false)
	@Column(name = "username", nullable = false, unique = true)
	private String username;

	@Column(name = "name")
	private String name;

	@Column(name = "email")
	private String email;

	@PastOrPresent
	@Column(name = "created_date")
	private Date createdDate;

	@PastOrPresent
	@Column(name = "last_login_date")
	private Date lastLoginDate;

	@Column(name = "about_description")
	private String aboutDescription;

	protected Profile() {

	}

	public Profile(String username, String name,
		String email, String aboutDescription) {
		super();
		this.username = username;
		this.name = name;
		this.email = email;
		this.createdDate = new Date();
		this.lastLoginDate = new Date();
		this.aboutDescription = aboutDescription;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getAboutDescription() {
		return aboutDescription;
	}

	public void setAboutDescription(String aboutDescription) {
		this.aboutDescription = aboutDescription;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
