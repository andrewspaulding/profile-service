package com.taledir.microservices;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import javax.validation.constraints.Pattern;

public class CreateProfileDto {

  @Size(max = 24)
  @NotBlank(message = "Username must be specified")
  @Pattern(regexp = "^[a-zA-Z0-9-]+$", 
      message = "Username must only contain letters, numbers, and - character, and cannot exceed 24 characters")
	private String username;

	private String name;

  @Email(message = "Please enter a valid email address")
  @NotBlank(message = "Email must be specified")
	private String email;

	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,16}$",
		message = "Password must be 8-16 characters, " +
			"contain one lowercase letter, one uppercase letter, one number, " +
			"one special character, and no whitespace characters.")
  @JsonProperty(access = Access.WRITE_ONLY)
  @NotBlank(message = "Password must be specified")
	private String password;

  private String aboutDescription;
  
  @NotBlank(message = "Please check the \"I'm not a robot\" checkbox")
  private String captchaResponse;

	public CreateProfileDto(String username, String name, String email, 
		String password, String aboutDescription, String captchaResponse) {
		this.username = username;
		this.name = name;
		this.email = email;
		this.password = password;
    this.aboutDescription = aboutDescription;
    this.captchaResponse = captchaResponse;
	}

    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username.toLowerCase().trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.toLowerCase().trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAboutDescription() {
		return aboutDescription;
	}

	public void setAboutDescription(String aboutDescription) {
		this.aboutDescription = aboutDescription;
	}

  public String getCaptchaResponse() {
    return captchaResponse;
  }

  public void setCaptchaResponse(String captchaResponse) {
    this.captchaResponse = captchaResponse;
  }

}
